"""
``JSON`` Data reader.
"""

import json
from collections.abc import Iterator, Mapping
from typing import TextIO, cast

from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class JSONDataReader:
    """
    ``JSON`` Data reader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.json import JSONDataReader
    >>> reader = JSONDataReader()
    >>> import io
    >>> data = '''{
    ... "#1": {
    ...     "name": "Galois",
    ...     "firstname": "Évariste"
    ...   },
    ... "#2": {
    ...     "name": "Wille",
    ...     "firstname": "Rudolf"
    ...   }
    ... }
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> pprint(individuals)
    {'#1': {'firstname': 'Évariste', 'name': 'Galois'},
     '#2': {'firstname': 'Rudolf', 'name': 'Wille'}}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``JSON`` data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        """
        individuals = json.load(data_file)
        if isinstance(individuals, Mapping):
            return individuals
        return {str(index): value for index, value in enumerate(individuals)}

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".json"])


def register() -> None:
    """
    Register an instance of a ``JSON`` data reader.
    """
    PopulationFactory.register_reader(JSONDataReader())
