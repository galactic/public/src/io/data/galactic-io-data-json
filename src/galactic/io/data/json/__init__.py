"""
JSON Data reader.
"""

from ._main import JSONDataReader, register

__all__ = ("JSONDataReader", "register")
