"""Population test module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.json import JSONDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, JSONDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
