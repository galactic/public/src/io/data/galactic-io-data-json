"""Reader test module."""

from pathlib import Path
from unittest import TestCase

from galactic.io.data.core import PopulationFactory


class JSONDataReaderTest(TestCase):
    def test_get_reader(self):
        pathname = Path(__file__).parent / "test.json"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "#1": {"name": "Galois", "firstname": "Évariste"},
                "#2": {"name": "Wille", "firstname": "Rudolf"},
            },
        )

        pathname = Path(__file__).parent / "test2.json"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "0": {"name": "Galois", "firstname": "Évariste"},
                "1": {"name": "Wille", "firstname": "Rudolf"},
            },
        )
