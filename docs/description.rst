==================
*JSON* data reader
==================

*galactic-io-data-json* is a
`JSON <https://en.wikipedia.org/wiki/JSON>`_ data reader plugin
for **GALACTIC**.

The file extension is ``.json``. The individuals are either represented by a
list or by a dictionary. For example:

.. code-block:: json
    :class: admonition

    {
      "#1": {
        "name": "Galois",
        "firstname": "Évariste"
      },
      "#2": {
        "name": "Wille",
        "firstname": "Rudolf"
      }
    }

This reader uses the ``json.load`` function of the
`python core library <https://docs.python.org/3/library/json.html#json.load>`_.

